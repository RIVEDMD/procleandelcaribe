<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table="products";
    protected  $primaryKey="id";

    protected $fillable = [
        'product','features','description','specs','price','units','images','created_at','updated_at'
    ];
}