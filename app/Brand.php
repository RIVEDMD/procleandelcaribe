<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected  $table="brands";
    protected  $primaryKey="id";

    protected $fillable = [
        'brand','description','image'
    ];

    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}