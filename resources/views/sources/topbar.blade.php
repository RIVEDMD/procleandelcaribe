<nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="/home">ProClean <span class="color-b">del Caribe</span></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            @if(Request::path() == "home")
              <a class="nav-link active" href="/home">Inicio</a>
            @else
              <a class="nav-link" href="/home">Inicio</a>
            @endif
            
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.html">Nosotros</a>
          </li>
          <li class="nav-item">
            @if(Request::path() == "shop")
              <a class="nav-link active" href="/shop">Comprar</a>
            @else
              <a class="nav-link" href="/shop">Comprar</a>
            @endif
            
          </li>
          <li class="nav-item">
            <a class="nav-link" href="blog-grid.html">Clientes</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Productos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/product/1">TaniCitrus</a>
              <a class="dropdown-item" href="blog-single.html">¡Lo más nuevo!</a>
              <a class="dropdown-item" href="agents-grid.html">¡Lo más vendido!</a>
              <a class="dropdown-item" href="agent-single.html">Ecologico</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.html">Contacto</a>
          </li>
        </ul>
      </div>
      <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-shopping-cart" aria-hidden="true"></span>
      </button>
    </div>
  </nav>