
<style scoped>
  .agregar{
    border-radius:50px;
  }

  .restar{
    border-radius:50px;
  }

  .eliminar{
    border-radius:50px;
  }
</style>

<div class="click-closed"></div>
  <!--/ Form Search Star /-->
  <div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Tú Carrito</h3>
    </div>
    <span class="close-box-collapse right-boxed ion-ios-close"></span>

    <div class="box-collapse-wrap form">
    <table class="table table-sm">
      <thead class="table-secondary">
        <tr>
          <td>Cant.</td>
          <td></td>
          <td>Producto</td>
          <td>Precio</td>
          <td></td>
          <td>Importe</td>
          <td></td>
        </tr>
      </thead>
      <tbody id="tBodyShopCart"> 
      </tbody>
    </table> 
  <hr>
    <p id="products"></p>
    <p id="total"></p>
    
      <button type="submit" class="btn btn-b col-lg-12">¡Comprar Ahora!</button>
    </div>
  </div>

  @section('scripts')
  <script>
  
    $(document).ready(function(){
      createTable();
      actions();
    });

    function createCart(){
      let cart = [{id:1,name:"tane",price:100,units:2},
                  {id:2,name:"tane",price:100,units:2},
                  {id:3,name:"tane",price:100,units:2}];

      localStorage.setItem('cart',JSON.stringify(cart));

      createTable();
    }

    function actions(){
      $(".btnAdd").on("click",function(){
        let cart = JSON.parse(localStorage.getItem('cart'));
        let id_product = $(this).attr("id-product");

        for (let i=0;cart.length > i;i++){
          let item = cart[i];
          if(item.id == id_product){
            item.units = item.units + 1;
          }
        }        

        localStorage.setItem('cart',JSON.stringify(cart));
        
        createTable();
      });

      $(".btnRemove").on("click",function(){
        let cart = JSON.parse(localStorage.getItem('cart'));
        let id_product = $(this).attr("id-product");

        for (let i=0;cart.length > i;i++){
          let item = cart[i];
          if(item.id == id_product){
            item.units = item.units - 1;
          }
        }        

        localStorage.setItem('cart',JSON.stringify(cart));
        
        createTable();
      });

      $(".btnDelete").on("click",function(){
        let cart = JSON.parse(localStorage.getItem('cart'));
        let id_product = $(this).attr("id-product");

        for (let i=0;cart.length > i;i++){
          let item = cart[i];
          if(item.id == id_product){
            cart.splice(i,1);
          }
        }        

        localStorage.setItem('cart',JSON.stringify(cart));
        
        createTable();
      });

    }

    function createTable(){
      let cart = JSON.parse(localStorage.getItem('cart'));
      let table = $("#tBodyShopCart");
      let productos = $("#products");
      let total = $("#total");
      let tbody = "";
      let total_units = 0;
      let total_amount = 0;

      if(cart.length > 0){
        for (let i=0;cart.length > i;i++){
        let item = cart[i];
        tbody += "<tr>";
        
        tbody += "<td>"+item.units+"</td>";
        tbody += "<td>x</td>";
        tbody += "<td>"+item.name+"</td>";
        tbody += "<td>$ "+item.price+"</td>";
        tbody += "<td>=</td>";
        tbody += "<td>$ "+(item.price * item.units).toFixed(2)+"</td>";
        tbody += '<td>'+
                  '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></button>'+
                  '<div class="dropdown-menu" aria-labelledby="navbarDropdown">'+
                   '<button class="dropdown-item btnAdd" id-product="'+item.id+'"><i class="fa fa-plus"></i> Agregar</button></li>'+
                   '<button class="dropdown-item btnRemove" id-product="'+item.id+'"><i class="fa fa-minus"></i> Quitar</button></li>'+
                   '<button class="dropdown-item btnDelete" id-product="'+item.id+'"><i class="fa fa-trash"></i> Eliminar</button></li>'+
                  '</div>'+
                  '</td>';

        tbody += "</tr>";

        total_units = total_units + item.units;
        total_amount = (total_amount + (item.price * item.units));

        } 

        table.html(tbody);
        productos.html("Productos: " + total_units);
        total.html("Total: $ " + total_amount.toFixed(2) + " MXN");
      }else{
        let table = $("#tBodyShopCart");
        let productos = $("#products");
        let total = $("#total");
        let tbody = "";

        tbody += "<tr>";

        tbody += "<td class='text-center' colspan='7'>";
        tbody += "Aún no tienes productos agregados";
        tbody += "</td>";
        tbody += "</tr>";

        table.html(tbody);
        productos.html("Productos: 0");
        total.html("Total: $ 0.00 MXN");

      }
      
    }
  </script>
  @endsection