
    
@yield('contenido')

<!--/ footer Star /-->
<section class="section-footer">
    <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4">
        <div class="widget-a">
            <div class="w-header-a">
            <h3 class="w-title-a text-brand">ProClean del Caribe</h3>
            </div>
            <div class="w-body-a">
            <p class="w-text-a color-text-a">
                Somos una empresa enfocada en erim minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat duis
                sed aute irure.
            </p>
            </div>
            <div class="w-footer-a">
            <ul class="list-unstyled">
                <li class="color-a">
                <span class="color-text-a">Correo .</span> oceanpacific@gmail.com</li>
                <li class="color-a">
                <span class="color-text-a">Teléfono .</span> 286 67 23</li>
                <li class="color-a">
                    <span class="color-text-a">Dirección .</span> Colonia Mexico. Calle 18 #231,
                    <br> ente 25 y 27.</li>
            </ul>
            </div>
        </div>
        </div>
        <div class="col-sm-12 col-md-4 section-md-t3">
        <div class="widget-a">
            <div class="w-header-a">
            <h3 class="w-title-a text-brand">Productos</h3>
            </div>
            <div class="w-body-a">
            <div class="w-body-a">
                <ul class="list-unstyled">
                <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="#">SaniCitrus</a>
                </li>
                <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="#">¡Lo nuevo!</a>
                </li>
                <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="#">¡Lo mas vendido!</a>
                </li>
                <li class="item-list-a">
                    <i class="fa fa-angle-right"></i> <a href="#">Ecologicos</a>
                </li>
                
                </ul>
            </div>
            </div>
        </div>
        </div>
        <div class="col-sm-12 col-md-4 section-md-t3">
        <div class="widget-a">
            <div class="w-header-a">
            <h3 class="w-title-a text-brand">Clientes</h3>
            </div>
            <div class="w-body-a">
            <ul class="list-unstyled">
                <li class="item-list-a">
                <i class="fa fa-angle-right"></i> <a href="#">Cliente 1</a>
                </li>
                <li class="item-list-a">
                <i class="fa fa-angle-right"></i> <a href="#">Cliente 2</a>
                </li>
                <li class="item-list-a">
                <i class="fa fa-angle-right"></i> <a href="#">Cliente 3</a>
                </li>
                <li class="item-list-a">
                <i class="fa fa-angle-right"></i> <a href="#">Cliente 4</a>
                </li>
            </ul>
            </div>
        </div>
        </div>
    </div>
    </div>
</section>
<footer>
    <div class="container">
    <div class="row">
        <div class="col-md-12">
        <nav class="nav-footer">
            <ul class="list-inline">
            <li class="list-inline-item">
                <a href="#">Inicio</a>
            </li>
            <li class="list-inline-item">
                <a href="#">Nosotros</a>
            </li>
            <li class="list-inline-item">
                <a href="#">Comprar</a>
            </li>
            <li class="list-inline-item">
                <a href="#">Productos</a>
            </li>
            <li class="list-inline-item">
                <a href="#">Contacto</a>
            </li>
            </ul>
        </nav>
        <div class="socials-a">
            <ul class="list-inline">
            <li class="list-inline-item">
                <a href="#">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a href="#">
                <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
            </li>
            </ul>
        </div>
        <div class="copyright-footer">
            <p class="copyright color-text-a">
            &copy; Copyright 2020
            <span class="color-a">Pro Clean Del Caribe</span> Todos los derechos reservados.
            </p>
        </div>
        <div class="credits">
            <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=EstateAgency
            -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
        </div>
    </div>
    </div>
</footer>
<!--/ Footer End /-->
