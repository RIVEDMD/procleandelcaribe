@extends('layouts.main')
@section('title')
{{$product->product}}
@endsection
@section('contenido')
<section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">{{$product->product}}</h1>
            <span class="color-text-a">{{$product->tiny_description}}</span>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/home">Inicio</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop">Marca</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop">Categoría</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop">Subcategoría</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Producto
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
<section class="property-single nav-arrow-b">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-lg-6 col-sm-12">
              <div id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
                @foreach(json_decode($product->images) as $image)
                <div class="carousel-item-b">
                  <img src="{{$image}}" alt="">
                </div>
                @endforeach
              </div>
            </div>
            <div class="col-sm-12 col-lg-6">
              <div class="property-price d-flex justify-content-center foo">
                <div class="card-header-c d-flex">
                  <div class="card-box-ico">
                    <span class="ion-money">$</span>
                  </div>
                  <div class="card-title-c align-self-center">
                    <h5 class="title-c">{{$product->price}}</h5>
                  </div>
                </div>
              </div>
              <div class="property-summary">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="title-box-d section-t4">
                      <h3 class="title-d">Características</h3>
                    </div>
                  </div>
                </div>
                <div class="summary-list">
                  <ul class="list">
                    @foreach(json_decode($product->features) as $feature)
                    <li class="d-flex justify-content-between">
                      <strong>{{$feature->title}}:</strong>
                      <span>{{$feature->def}}</span>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between">
            <div class="col-lg-12 section-md-t3">
              <div class="row">
                <div class="col-sm-12">
                  <div class="title-box-d">
                    <h3 class="title-d">Descripción</h3>
                  </div>
                </div>
              </div>
              <div class="property-description">
                <p class="description color-text-a">
                  {{$product->description}}
                </p>
              </div>
              <div class="row section-t3">
                <div class="col-sm-12">
                  <div class="title-box-d">
                    <h3 class="title-d">Especificaciones</h3>
                  </div>
                </div>
              </div>
              <div class="amenities-list color-text-a">
                <ul class="list-a no-margin">
                  @foreach(json_decode($product->specs) as $spec)
                    <li>{{$spec}}</li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row section-t3">
            <div class="col-sm-12">
              <div class="title-box-d">
                <h3 class="title-d">También compraron</h3>
              </div>
            </div>
          </div>
          <div id="property-carousel" class="owl-carousel owl-theme">
            @foreach ($also_bought as $item)
              <div class="carousel-item-b">
                <div class="card-box-a card-shadow">
                  <div class="img-box-a">
                    <img src="https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870" alt="" class="img-a img-fluid">
                  </div>
                  <div class="card-overlay">
                    <div class="card-overlay-a-content">
                      <div class="card-header-a">
                        <h2 class="card-title-a">
                          <a href="/product/{{$item->id}}">{{$item->product}}</a>
                        </h2>
                      </div>
                      <div class="card-body-a">
                        <div class="price-box d-flex">
                          <span class="price-a">Agregar | $ {{$item->price}}</span>
                        </div>
                        <a href="/product/{{$item->id}}" class="link-a">Ver producto
                          <span class="ion-ios-arrow-forward"></span>
                        </a>
                      </div>
                      <div class="card-footer-a">
                        <ul class="card-info d-flex justify-content-around">
                          <li>
                            <h4 class="card-info-title">{{json_decode($item->features)[0]->title}}</h4>
                            <span>{{json_decode($item->features)[0]->def}}</span>
                          </li>
                          <li>
                            <h4 class="card-info-title">{{json_decode($item->features)[1]->title}}</h4>
                            <span>{{json_decode($item->features)[1]->def}}</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection