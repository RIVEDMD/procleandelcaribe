@extends('layouts.main')
@section('title')
    Bienvenido
@endsection
@section('contenido')
<!--/ Carousel Star /-->
<div class="intro intro-carousel">
  <div id="carousel" class="owl-carousel owl-theme">
    <div class="carousel-item-a intro-item bg-image" style="background-image: url(../imagenes/logo_d_f.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">
                   
                    <br>  Divertidas Piñatas Temáticas</p>
                  <h1 class="intro-title mb-4">
                    ProClean del<span class="color-df">Caribe</span>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">¡Comprar Ahora!</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item-a intro-item bg-image" style="background-image: url(../imagenes/logo_d_f.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">
                    <br>  Divertidas Piñatas Temáticas</p>
                  <h1 class="intro-title mb-4">
                    ProClean del<span class="color-df">Caribe</span>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">¡Comprar Ahora!</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item-a intro-item bg-image" style="background-image: url(../imagenes/logo_d_f.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">
                    <br>  Divertidas Piñatas Temáticas</p>
                  <h1 class="intro-title mb-4">
                    Dulce<span class="color-df">Fiesta</span>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">¡Comprar Ahora!</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ Carousel end /-->

<section class="intro-single">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="title-single-box">
          <h1 class="title-single">Nosotros</h1>
        </div>
      </div>

    </div>
  </div>
</section>
<!--/ Intro Single End /-->

<!--/ About Star /-->
<section class="section-about">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="about-img-box">
          <img src="../imagenes/local.jpg" alt="" class="img-fluid">
        </div>
        <div class="sinse-box">
          <h3 class="sinse-title">Dulce Fiesta
            <span></span>
            <br> Desde 2020</h3>
          <p>Piñatas y más</p>
        </div>
      </div>
      <div class="col-md-12 section-t8">
        <div class="row">
          <div class="col-md-6 col-lg-5">
            <img src="../imagenes/local.jpg" alt="" class="img-fluid">
          </div>
          <div class="col-lg-2  d-none d-lg-block">
            <div class="title-vertical d-flex justify-content-start">
              <span>EstateAgency Exclusive Property</span>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 section-md-t3">
            <div class="title-box-d">
              <h3 class="title-d">Sed
                <span class="color-d">porttitor</span> lectus
                <br> nibh.</h3>
            </div>
            <p class="color-text-a">
              Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget
              consectetur sed, convallis
              at tellus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum
              ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit
              neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
            </p>
            <p class="color-text-a">
              Sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.
              Mauris blandit aliquet
              elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed,
              convallis at tellus.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Services Star /-->
<section class="section-services section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-wrap d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Servicios</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            <div class="card-box-ico">
              <span class="fa fa-gamepad"></span>
            </div>
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Fiestas</h2>
            </div>
          </div>
          <div class="card-body-c">
            <p class="content-c">
              Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
              convallis a pellentesque
              nec, egestas non nisi.
            </p>
          </div>
          <div class="card-footer-c">
            <a href="#" class="link-c link-icon">Read more
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            <div class="card-box-ico">
              <span class="fa fa-gift"></span>
            </div>
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Regalos</h2>
            </div>
          </div>
          <div class="card-body-c">
            <p class="content-c">
              Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Mauris blandit
              aliquet elit, eget tincidunt
              nibh pulvinar a.
            </p>
          </div>
          <div class="card-footer-c">
            <a href="#" class="link-c link-icon">Read more
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            <div class="card-box-ico">
              <span class="fa fa-birthday-cake"></span>
            </div>
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Encargos</h2>
            </div>
          </div>
          <div class="card-body-c">
            <p class="content-c">
              Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
              convallis a pellentesque
              nec, egestas non nisi.
            </p>
          </div>
          <div class="card-footer-c">
            <a href="#" class="link-c link-icon">Read more
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Services End /-->

<!--/ Property Star /-->
<section class="section-property section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-wrap d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">¡Lo Nuevo!</h2>
          </div>
          <div class="title-link">
            <a href="property-grid.html">Ver más
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id="property-carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-b">
        <div class="card-box-a card-shadow">
          <div class="img-box-a">
            <img src="../imagenes/dos.jpg" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h2 class="card-title-a">
                  <a href="property-single.html">206 Mount
                    <br /> Olive Road Two</a>
                </h2>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">rent | $ 12.000</span>
                </div>
                <a href="#" class="link-a">Click here to view
                  <span class="ion-ios-arrow-forward"></span>
                </a>
              </div>
              <div class="card-footer-a">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                    <h4 class="card-info-title">Area</h4>
                    <span>340m
                      <sup>2</sup>
                    </span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>2</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>4</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Garages</h4>
                    <span>1</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-b">
        <div class="card-box-a card-shadow">
          <div class="img-box-a">
            <img src="../imagenes/dos.jpg" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h2 class="card-title-a">
                  <a href="property-single.html">157 West
                    <br /> Central Park</a>
                </h2>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">rent | $ 12.000</span>
                </div>
                <a href="property-single.html" class="link-a">Click here to view
                  <span class="ion-ios-arrow-forward"></span>
                </a>
              </div>
              <div class="card-footer-a">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                    <h4 class="card-info-title">Area</h4>
                    <span>340m
                      <sup>2</sup>
                    </span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>2</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>4</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Garages</h4>
                    <span>1</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-b">
        <div class="card-box-a card-shadow">
          <div class="img-box-a">
            <img src="../imagenes/dos.jpg" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h2 class="card-title-a">
                  <a href="property-single.html">245 Azabu
                    <br /> Nishi Park let</a>
                </h2>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">rent | $ 12.000</span>
                </div>
                <a href="property-single.html" class="link-a">Click here to view
                  <span class="ion-ios-arrow-forward"></span>
                </a>
              </div>
              <div class="card-footer-a">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                    <h4 class="card-info-title">Area</h4>
                    <span>340m
                      <sup>2</sup>
                    </span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>2</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>4</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Garages</h4>
                    <span>1</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-b">
        <div class="card-box-a card-shadow">
          <div class="img-box-a">
            <img src="../imagenes/dos.jpg" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h2 class="card-title-a">
                  <a href="property-single.html">204 Montal
                    <br /> South Bela Two</a>
                </h2>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">rent | $ 12.000</span>
                </div>
                <a href="property-single.html" class="link-a">Click here to view
                  <span class="ion-ios-arrow-forward"></span>
                </a>
              </div>
              <div class="card-footer-a">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                    <h4 class="card-info-title">Area</h4>
                    <span>340m
                      <sup>2</sup>
                    </span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>2</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>4</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Garages</h4>
                    <span>1</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Property End /-->

<!--/ Agents Star /-->
<section class="section-agents section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-wrap d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Nuestras Marcas</h2>
          </div>
          <div class="title-link">
            <a href="/shop/brands">Todas las marcas
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card-box-d">
          <div class="card-img-d">
            <img src="../imagenes/skwinkles.jpg" alt="" class="img-d img-fluid">
          </div>
          <div class="card-overlay card-overlay-hover">
            <div class="card-header-d">
              <div class="card-title-d align-self-center">
                <h3 class="title-d">
                  <a href="agent-single.html" class="link-two">Skwinkles
                    <br> </a>
                </h3>
              </div>
            </div>
            <div class="card-body-d">
              <p class="content-d color-text-a">
                Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
              </p>
              <div class="info-agents color-a">
                <p>
                  <strong>Phone: </strong> +54 356 945234</p>
                <p>
                  <strong>Email: </strong> agents@example.com</p>
              </div>
            </div>
            <div class="card-footer-d">
              <div class="socials-footer d-flex justify-content-center">
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-dribbble" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="card-box-d">
          <div class="card-img-d">
            <img src="../imagenes/skwinkles.jpg" alt="" class="img-d img-fluid">
          </div>
          <div class="card-overlay card-overlay-hover">
            <div class="card-header-d">
              <div class="card-title-d align-self-center">
                <h3 class="title-d">
                  <a href="agent-single.html" class="link-two">Skwinkles
                    <br> </a>
                </h3>
              </div>
            </div>
            <div class="card-body-d">
              <p class="content-d color-text-a">
                Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
              </p>
              <div class="info-agents color-a">
                <p>
                  <strong>Phone: </strong> +54 356 945234</p>
                <p>
                  <strong>Email: </strong> agents@example.com</p>
              </div>
            </div>
            <div class="card-footer-d">
              <div class="socials-footer d-flex justify-content-center">
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-dribbble" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="card-box-d">
          <div class="card-img-d">
            <img src="../imagenes/skwinkles.jpg" alt="" class="img-d img-fluid">
          </div>
          <div class="card-overlay card-overlay-hover">
            <div class="card-header-d">
              <div class="card-title-d align-self-center">
                <h3 class="title-d">
                  <a href="agent-single.html" class="link-two">Skwinkles
                    <br> </a>
                </h3>
              </div>
            </div>
            <div class="card-body-d">
              <p class="content-d color-text-a">
                Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
              </p>
              <div class="info-agents color-a">
                <p>
                  <strong>Phone: </strong> +54 356 945234</p>
                <p>
                  <strong>Email: </strong> agents@example.com</p>
              </div>
            </div>
            <div class="card-footer-d">
              <div class="socials-footer d-flex justify-content-center">
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-one">
                      <i class="fa fa-dribbble" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Agents End /-->

<!--/ News Star /-->
<section class="section-news section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-wrap d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Lo más vendido</h2>
          </div>
          <div class="title-link">
            <a href="blog-grid.html">Ver más
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id="new-carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="../imagenes/pac-man.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">House</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">House is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="../imagenes/pac-man.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="../imagenes/pac-man.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Park</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Park is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="../imagenes/pac-man.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="#">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ News End /-->

<!--/ Testimonials Star /-->
<section class="section-testimonials section-t8 nav-arrow-a">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-wrap d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Clientes contentos</h2>
          </div>
        </div>
      </div>
    </div>
    <div id="testimonial-carousel" class="owl-carousel owl-arrow">
      <div class="carousel-item-a">
        <div class="testimonials-box">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <div class="testimonial-img">
                <img src="img/testimonial-1.jpg" alt="" class="img-fluid">
              </div>
            </div>
            <div class="col-sm-12 col-md-6">
              <div class="testimonial-ico">
                <span class="ion-ios-quote"></span>
              </div>
              <div class="testimonials-content">
                <p class="testimonial-text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                  debitis hic ber quibusdam
                  voluptatibus officia expedita corpori.
                </p>
              </div>
              <div class="testimonial-author-box">
                <img src="img/mini-testimonial-1.jpg" alt="" class="testimonial-avatar">
                <h5 class="testimonial-author">Albert & Erika</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-a">
        <div class="testimonials-box">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <div class="testimonial-img">
                <img src="img/testimonial-2.jpg" alt="" class="img-fluid">
              </div>
            </div>
            <div class="col-sm-12 col-md-6">
              <div class="testimonial-ico">
                <span class="ion-ios-quote"></span>
              </div>
              <div class="testimonials-content">
                <p class="testimonial-text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                  debitis hic ber quibusdam
                  voluptatibus officia expedita corpori.
                </p>
              </div>
              <div class="testimonial-author-box">
                <img src="img/mini-testimonial-2.jpg" alt="" class="testimonial-avatar">
                <h5 class="testimonial-author">Pablo & Emma</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Testimonials End /-->
  <section class="intro-single">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="title-single-box">
          <h1 class="title-single">Contactanos</h1>
          <span class="color-text-a">Aut voluptas consequatur unde sed omnis ex placeat quis eos. Aut natus officia corrupti qui autem fugit consectetur quo. Et ipsum eveniet laboriosam voluptas beatae possimus qui ducimus. Et voluptatem deleniti. Voluptatum voluptatibus amet. Et esse sed omnis inventore hic culpa.</span>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Intro Single End /-->

<!--/ Contact Star /-->
<section class="contact">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="contact-map box">
          <div id="map" class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1521.5217279345!2d-89.60428994495025!3d20.998725283291407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e0!4m3!3m2!1d20.998845199999998!2d-89.6040038!4m0!5e0!3m2!1sen!2smx!4v1548447215300"
              width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="col-sm-12 section-t8">
        <div class="row ">
          <div class="col-md-12 d-flex section-md-t3">
            <div class="icon-box section-b2">
              <div class="icon-box-icon">
                <span class="ion-ios-paper-plane"></span>
              </div>
              <div class="icon-box-content table-cell">
                <div class="icon-box-title">
                  <h4 class="icon-title">Escribenos</h4>
                </div>
                <div class="icon-box-content">
                  <p class="mb-1">Correo.
                    <span class="color-a">dulcefiesta@gmail.com</span>
                  </p>
                  <p class="mb-1">Teléfono.
                    <span class="color-a">+54 356 945234</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="icon-box section-b2">
              <div class="icon-box-icon">
                <span class="ion-ios-pin"></span>
              </div>
              <div class="icon-box-content table-cell">
                <div class="icon-box-title">
                  <h4 class="icon-title">Estamos cerca de ti</h4>
                </div>
                <div class="icon-box-content">
                  <p class="mb-1">
                  Colonia Mexico. Calle 18 #231,
                    <br> ente 25 y 27.
                  </p>
                </div>
              </div>
            </div>
            <div class="icon-box section-b2">
              <div class="icon-box-icon">
                <span class="ion-ios-redo"></span>
              </div>
              <div class="icon-box-content table-cell">
                <div class="icon-box-title">
                  <h4 class="icon-title">Encuentranos en</h4>
                </div>
                <div class="icon-box-content">
                  <div class="socials-footer">
                    <ul class="list-inline">
                      <li class="list-inline-item">
                        <a href="#" class="link-one">
                          <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a href="#" class="link-one">
                          <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection