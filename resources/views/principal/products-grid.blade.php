@extends('layouts.main')
@section('title')
    productos
@endsection
@section('contenido')
<!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">productos</h1>
            <span class="color-text-a">productos</span>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/home">Inicio</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop">Comprar</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop/brands">Marcas</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop/brand/categories">Categorías</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Productos
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ Product Grid Star /-->
  
  <section class="property-grid grid">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="grid-option">
            <form>
              <select class="custom-select">
                <option selected>All</option>
                <option value="1">New to Old</option>
                <option value="2">For Rent</option>
                <option value="3">For Sale</option>
              </select>
            </form>
          </div>
        </div>
        @foreach ($products as $product)
        <div class="col-md-4">
          <div class="card-box-a card-shadow">
            <div class="img-box-a">
              <img src="{{json_decode($product->images)[1]}}" alt="" class="img-a img-fluid">
            </div>
            <div class="card-overlay">
              <div class="card-overlay-a-content">
                <div class="card-header-a">
                  <h2 class="card-title-a">
                    <a href="/product/{{$product->id}}">{{$product->product}}</a>
                  </h2>
                </div>
                <div class="card-body-a">
                  <div class="price-box d-flex">
                    <span class="price-a">Agregar | $ {{$product->price}}</span>
                  </div>
                  <a href="/product/{{$product->id}}" class="link-a">Ver producto
                    <span class="ion-ios-arrow-forward"></span>
                  </a>
                </div>
                <div class="card-footer-a">
                  <ul class="card-info d-flex justify-content-around">
                    <li>
                      <h4 class="card-info-title">{{json_decode($product->features)[0]->title}}</h4>
                      <span>{{json_decode($product->features)[0]->def}}</span>
                    </li>
                    <li>
                      <h4 class="card-info-title">{{json_decode($product->features)[1]->title}}</h4>
                      <span>{{json_decode($product->features)[1]->def}}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
      <div class="row">
        <div class="col-sm-12">
          <nav class="pagination-a">
            <ul class="pagination justify-content-end">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">
                  <span class="ion-ios-arrow-back"></span>
                </a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">1</a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">2</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">3</a>
              </li>
              <li class="page-item next">
                <a class="page-link" href="#">
                  <span class="ion-ios-arrow-forward"></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </section>
  @endsection
  @section('scripts')
    <script>
      $(document).ready(function(){
        ('#')
      });
    </script>
  @endsection