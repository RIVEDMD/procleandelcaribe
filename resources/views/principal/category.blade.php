@extends('layouts.main')
@section('title')
    {{$brand->brand}}
@endsection
@section('contenido')
<!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">{{$brand->brand}}</h1>
            <span class="color-text-a">{{$brand->description}}</span>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/home">Inicio</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop">Comprar</a>
              </li>
              <li class="breadcrumb-item">
                <a href="/shop/brands">Marcas</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Categorías
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ Brand Grid Star /-->
  <section class="property-grid grid">
    <div class="container">
      <div class="row">
        @foreach ($brand->categories as $category)
            <div class="col-md-6">
          <div class="card-box-d">
            <div class="card-img-d">
              <img src="{{$category->image}}" alt="" class="img-d img-fluid">
            </div>
            <div class="card-overlay card-overlay-hover">
              <div class="card-header-d">
                <div class="card-title-d align-self-center">
                  <h3 class="title-d">
                    <a href="/shop/brand/category/products/{{$category->id}}" class="link-two">{{$category->category}}</a>
                  </h3>
                </div>
              </div>
              <div class="card-body-d">
                <p class="content-d color-text-a">
                  {{$category->description}}
                </p>
              </div>
              <div class="card-footer-d">
                <div class="socials-footer d-flex justify-content-center">
                  <h5 class="title-d">
                    <a href="/shop/brand/category/products/{{$category->id}}" class="link-two">Ver Productos <span class="ion-ios-arrow-forward"></span></a>
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
  @endsection
  @section('scripts')
    <script>
      $(document).ready(function(){
        ('#')
      });
    </script>
  @endsection